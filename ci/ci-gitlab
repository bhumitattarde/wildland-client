#!/usr/bin/env bash

set -ex

. /home/user/env/bin/activate
. ci/install-inplace gitlab

WL='python3 -m coverage run -p ./wl'

#setup
ROOT="$HOME/wildland"
MNT_DIR_1="gitlab-mnt-1"
MNT_DIR_2="gitlab-mnt-2"

# Create and mount two GitLab containers: with and without specified project ID
$WL user create gitlab-user
$WL container create GitLab-p --path "/$MNT_DIR_1"
$WL storage create gitlab --container GitLab-p --personal-token $PERSONAL_GITLAB_TOKEN --projectid 27483617
$WL start
$WL container mount --with-subcontainers GitLab-p

# List full tree
time -p chronic tree "$ROOT"

# List /wildland directory
time -p chronic ls "$ROOT"

# List contents of the project directory
time -p chronic ls "$ROOT"/projects/ci\ example\ project/

# List contents of the timeline directory for the project
time -p test -d "$ROOT"/timeline/2021/06/16

# Checking the categories permutation
time -p test -d "$ROOT"/projects/ci\ example\ project/@labels/feature/

#labels with 2 issues
time -p test -f "$ROOT"/labels/backends/issue\ 1/issue\ 1.md
time -p test -f "$ROOT"/labels/backends/issue\ 2/issue\ 2.md

#labes with a single issue
time -p test -f "$ROOT"/labels/feature/issue\ 1/issue\ 1.md
time -p test -f "$ROOT"/labels/welcome/issue\ 2/issue\ 2.md

# Read contents of files
time -p chronic grep 'issue' "$ROOT"/projects/ci\ example\ project/issue\ 2/issue\ 2.md

# Unmount the container
time -p chronic $WL container unmount GitLab-p

$WL container create GitLab --path "/$MNT_DIR_2"
$WL storage create gitlab --container GitLab --personal-token $PERSONAL_GITLAB_TOKEN

# Mount a GitLab container (without specified project ID)
$WL container mount --with-subcontainers GitLab

# List contents of the timeline directory for the project
time -p test -d "$ROOT"/timeline/2021/06/16

#labes with a single issue
time -p test -f "$ROOT"/labels/feature/issue\ 1/issue\ 1.md
time -p test -f "$ROOT"/labels/welcome/issue\ 2/issue\ 2.md

# Unmount the container
time -p chronic $WL container unmount GitLab

# Test storage template creation
$WL template create gitlab --personal-token $PERSONAL_GITLAB_TOKEN --projectid 27483617 mygitlab
$WL container create gitlab-template --template mygitlab
$WL container mount gitlab-template

# Verify that the template creation is working
time -p chronic tree $ROOT

$WL container unmount gitlab-template
