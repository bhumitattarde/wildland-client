#!/usr/bin/env bash

set -ex

. /home/user/env/bin/activate
. ci/install-inplace imap

# pytest

pytest -v \
       --cov plugins/imap/wildland_imap \
       plugins/imap
mv /tmp/.coverage.wildland /tmp/.coverage.wildland.pytest

# Setup
ROOT="$HOME/wildland"
MNT_DIR_1="imap-mnt-1"
MNT_PATH_1="$ROOT/$MNT_DIR_1"

# Configure a mail server and populate a mailbox
for sender in 1 2 3; do
    for msg in 1 2 3; do
        ./ci/imap-mail-make /home/user/Maildir sender${sender}@dev.null \
                            "Test $msg" \
                            "`date --date=2020-0${msg}-0${msg}\ 11:13`" \
                            "Test number ${msg}"
    done
done
# Create and mount an IMAP container
WL='python3 -m coverage run -p ./wl'

sudo /etc/init.d/dovecot start

sudo chmod 0666 /dev/fuse

$WL user create User
$WL container create  Imap --path "/$MNT_DIR_1"
$WL storage create imap --container Imap --host 127.0.0.1 --login user \
     --password test --trusted --no-ssl
$WL start
$WL container mount --with-subcontainers Imap

# List full tree
time -p chronic tree "$ROOT"

# List contents of sender2 dir
time -p chronic ls -l "$ROOT"/users/sender1@dev.null/sender/

# List contents of February timeline dir
time -p chronic ls -l "$ROOT"/timeline/2020/02/02

# Read contents of a files
time -p chronic grep 'Test number 1' "$ROOT"/users/sender2@dev.null/sender/Test\ 1*/main_body.txt
time -p chronic grep 'Test number 2' "$ROOT"/users/sender2@dev.null/sender/Test\ 2*/main_body.txt
time -p chronic grep 'Test number 1' "$ROOT"/timeline/2020/01/01/Test\ 1*/main_body.txt

# Unmount container
time -p chronic $WL container unmount Imap

